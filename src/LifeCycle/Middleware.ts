import { Request } from "./Request";
import { Response } from "./Response";

export interface Middleware {
	(request: Request): Promise<Response> | null;
}