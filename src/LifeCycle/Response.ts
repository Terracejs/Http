import { HttpStatus } from "./HttpStatus";

export interface Response {
	status: HttpStatus,
	headers: Map<string, string>,
	body: any
}