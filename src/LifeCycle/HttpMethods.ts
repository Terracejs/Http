export enum HttpMethods {
	HEAD,
	TRACE,
	CONNECT,
	GET,
	POST,
	PUT,
	PATCH,
	DELETE
}