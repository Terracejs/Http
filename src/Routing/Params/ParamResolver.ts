export interface ParamResolver<T> {
	(identifier: string): T
}