import { RequestParam, CreateParamDecorator } from "./RequestParam";
import { QueryMetadata } from "../Constants";

export interface QueryParam extends RequestParam { }

export const QueryParam = CreateParamDecorator(QueryMetadata);