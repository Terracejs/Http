import { RequestParam, CreateParamDecorator } from "./RequestParam";
import { BodyMetadata } from "..";

export interface BodyParam extends RequestParam {}

export const BodyParam = CreateParamDecorator(BodyMetadata);