import { ParamResolver } from "./ParamResolver";
import { ParameterMetadata } from "../Constants";
import { RequestParam, CreateParamDecorator } from "./RequestParam";

export interface RouteParam<T> extends RequestParam {
	/**
	 * A function to resolve a parameter
	 * based on an identifier
	 * 
	 * @type ParamResolver<T>
	 */
	resolver?: ParamResolver<T>;

	/**
	 * A regular expresion for validating a url parameter
	 * This is used to help validate the full uri.
	 * @default number /^\d+(\.?\d+)*$/, boolean ^[0-1]{1}$|^(true|false){1}$, string and symbol ^.*$
	 */
	validator?: RegExp;
}

export function staticResolver(identifier: string) {
	return identifier;
}

export const RouteParam = CreateParamDecorator(ParameterMetadata, function (config: RouteParam<any>) {
	if (config.resolver === undefined)
		config.resolver = staticResolver;
});