const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
const ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
	let fnStr = func.toString().replace(STRIP_COMMENTS, '');
	let result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
	if (result === null)
		result = [];
	return result;
}

export interface RequestParam {
	/**
	 * The name of the parameter. Defaults
	 * to the function parameter name.
	 * 
	 * @type string
	 */
	name?: string;

	/**
	 * A function to validate
	 * the parameter type
	 * 
	 * @type (any) => boolean | RegExp
	 */
	validator?: ((obj: any) => boolean) | RegExp;
}

export type decorator = <T extends RequestParam>(metadataKey: Symbol, validate?: <T>(config: T) => void)
	=> (config?: T) => (target: any, name: string, index: number) => void;

export const ident = function (obj) { return true };

function decorator <T extends RequestParam>(metadataKey: Symbol, validate?: <T>(config: T) => void) {
	return function (config?: T) {
		return function (target: any, name: string, index: number) {
			if (!Reflect.hasMetadata(metadataKey, target)) {
				Reflect.defineMetadata(metadataKey, [], target[name]);
			}

			let options: RequestParam = config || {
				name: getParamNames(target[name])[index],
				validator: ident
			};

			if (options.name === undefined) options.name = getParamNames(target[name])[index];
			if (options.validator === undefined) options.validator = ident;

			if (validate) validate(options);
	
			let meta: Array<RequestParam> = Reflect.getMetadata(metadataKey, target[name]);
			meta[index] = options;
		}
	}
}

export const CreateParamDecorator: decorator = decorator;