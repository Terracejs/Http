import { Resource, ParamResolver, RouterMetada } from ".";
import { Middleware } from "..";

export interface Router {
	host: Array<string>;
	staticDir?: Array<string>;
	mountResources?: string;
	resources?: Array<any>;
	middleware?: Middleware[];
	resolvers?: Map<string, ParamResolver<any>>; 
}

export const Router = function (config?: Router) {
	return function (target: new (...args: Array<any>) => any) {
		if (!config) config = { host: [".*"] };
		Reflect.defineMetadata(RouterMetada, config, target);
	}
}