import { Request, Middleware, HttpMethods } from "../LifeCycle";
import { RouteMetadata, ParameterMetadata } from "./Constants";

export interface Route {
	method: HttpMethods,
	path: string,
	middleware?: Array<Middleware>
}

export const Route = function (config: Route) {
	return function (target: any, name: string, descriptor: PropertyDescriptor) {
		if (target.constructor.name === "Function") throw new TypeError("Must be placed on class method");

		if (!Reflect.hasMetadata(RouteMetadata, target)) {
			Reflect.defineMetadata(RouteMetadata, new Map(), target);
		}

		let metaData: Map<String, Route> = Reflect.getMetadata(RouteMetadata, target);

		metaData.set(name, config);
	}
}
//console.log(Reflect.getMetadata("design:paramtypes", Test));