import { RouteMetadata, ParameterMetadata, QueryMetadata, BodyMetadata, ResourceMetadata } from "./Constants";
import { Middleware, Route, ParamResolver, RouteParam, staticResolver, QueryParam, ident, BodyParam } from "..";

export interface Resource {
	basePath: string;
	middleware?: Middleware[];
	validators?: Map<string, RegExp | ((obj) => boolean)>;
	resolvers?: Map<string, ParamResolver<any>>
}

export interface PathGroup {
	route: Route;
	constructor: new () => any;
	handler: string;
	queryParams: Array<QueryParam>;
	routeParams: Array<RouteParam<any>>;
	bodyParams: Array<BodyParam>;
}

export const Resource = function (config: Resource) {
	return function (target: new (...args: Array<any>) => any) {
		if (!Reflect.hasMetadata(RouteMetadata, target.prototype)) return;

		let routes: Map<string, Route> = Reflect.getMetadata(RouteMetadata, target.prototype);
		let resourceMeta: PathGroup[] = [];

		for (let [name, route] of routes) {
			route.path = config.basePath + route.path;
			route.middleware = [...(config.middleware || []), ...(route.middleware || [])];

			let group = {
				route: route,
				constructor: target,
				handler: name,
				queryParams: [],
				routeParams: [],
				bodyParams: []
			};

			if (Reflect.hasMetadata(ParameterMetadata, target.prototype[name])) {
				let params: [RouteParam<any>] = Reflect.getMetadata(ParameterMetadata, target.prototype[name]);
				for (let meta of params) {
					if (config.resolvers && meta.resolver === staticResolver && config.resolvers.has(meta.name)) {
						meta.resolver = config.resolvers.get(name);
					}

					if (config.validators && !(meta.validator instanceof RegExp) && config.validators.has(meta.name)) {
						let validator = config.validators.get(meta.name);
						if (validator instanceof RegExp)
							meta.validator = validator;
					}

					group.routeParams.push(meta);
				}
			}

			let query = Reflect.hasMetadata(QueryMetadata, target.prototype[name]);
			let body = Reflect.hasMetadata(BodyMetadata, target.prototype[name]);

			if (query || body){
				let params: [QueryParam | BodyParam] = Reflect.getMetadata(QueryMetadata, target.prototype[name])
					|| Reflect.getMetadata(BodyMetadata, target.prototype[name]);

				for (let meta of params) {
					if (config.validators && meta.validator === ident && config.validators.has(meta.name)) {
						meta.validator = config.validators.get(meta.name);
					}
					
					if (body) group.bodyParams.push(meta);
					if (query) group.queryParams.push(meta);
				}
			}

			resourceMeta.push(group);
		}
		
		Reflect.defineMetadata(ResourceMetadata, resourceMeta, target);
	}
}