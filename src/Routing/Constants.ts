export const RouteMetadata = Symbol("Routemetadata");
export const ParameterMetadata = Symbol("ParameterMetadata");
export const ResourceMetadata = Symbol("ControllerMetadata");
export const QueryMetadata = Symbol("QueryMetadata");
export const BodyMetadata = Symbol("BodyMetadata");
export const RouterMetada = Symbol("RouterMetadata");