require("reflect-metadata");
import { createServer } from "http";
import { Router, Resource, Route, HttpMethods, RouterMetada, RouteMetadata, ResourceMetadata, PathGroup } from ".";

export * from "./Routing";
export * from "./LifeCycle";

@Resource({
	basePath: "/TestResource"
})
class TestResource {
	@Route({
		path: "/getRoute",
		method: HttpMethods.GET
	})
	getRoute() {
		return "Hello World";
	}
}


@Router({
	host: ["./*"],
	resources: [TestResource]
})
class Test {

}

let meta = Reflect.getMetadata(RouterMetada, Test);
let resources: Array<Array<PathGroup>> = [];

for (let resource of meta.resources) {
	let meta2 = Reflect.getMetadata(ResourceMetadata, resource);
	resources.push(meta2);
}

console.log(resources);

const server = createServer((req, res) => {
	for (let resource of resources) {
		for (let routeGroup of resource) {
			if (routeGroup.route.path === req.url) {
				let temp = new routeGroup.constructor();
				res.end(temp.getRoute());
				return;
			}
		}
	}

	res.end("Here");
});

server.on('clientError', (err, socket) => {
  socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});

server.listen(8080);