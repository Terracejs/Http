[![coverage report](https://gitlab.com/Terracejs/Http/badges/master/coverage.svg)](https://gitlab.com/Terracejs/Http/commits/master)
[![pipeline status](https://gitlab.com/Terracejs/Http/badges/master/pipeline.svg)](https://gitlab.com/Terracejs/Http/commits/master)
The Http package for the Terracejs framework.