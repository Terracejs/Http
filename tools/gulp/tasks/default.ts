import { task } from 'gulp';
const gulp = require('gulp');

function help(done) {
	let taskList = gulp.tree();

		console.log(`\nAvailable tasks:\n    ${taskList.nodes.join('\n    ')}\n`);
		done();
}

task('default', help);

task('help', help);