import { task, watch, series, parallel } from "gulp";
import * as path from "path";

import { SOURCE_ROOT, DIST_ROOT } from "../constants";
import { mochaTask, tsBuildTask } from "../taskHelpers";

function run(done) {
	watch(path.join(SOURCE_ROOT, '**/*.ts'), series('build:development'));
	done();
}

task("watch:source", series('build:development', run));