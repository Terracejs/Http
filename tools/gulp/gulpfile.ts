import './tasks/clean';
import './tasks/build';
import './tasks/release';
import './tasks/default';
import './tasks/watch';