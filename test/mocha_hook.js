require("reflect-metadata");
let ts_node = require("ts-node");
let tsconfig_paths = require("tsconfig-paths");
tsconfig_paths.register({
	baseUrl: `${__dirname}/../src`,
	paths: require(`${__dirname}/tsconfig.json`).compilerOptions.paths
});
ts_node.register({
	project: `${__dirname}/tsconfig.json` 
});
require("source-map-support/register");