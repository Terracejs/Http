import * as mocha from "mocha";
import * as rimraf from "rimraf";
import { PROJECT_ROOT } from "../tools/gulp/constants";

process.env.APP_ENV = "testing";

before(function () {
});

after(function (done) {
	rimraf(`${PROJECT_ROOT}/.ts-node`, err => done(err))
});