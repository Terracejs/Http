import * as assert from "assert";
import {
	Resource,
	Route,
	RouteMetadata,
	ResourceMetadata,
	HttpMethods,
	Request,
	RouteParam,
	QueryParam,
	BodyParam
} from "@Terracejs/Http";

describe("Resource Annotation", function () {
	it("Doesn't apply metadata when there is no route metadata on the class", function () {
		@Resource({ basePath: "/" })
		class Test { }

		let actualHasMetadata = Reflect.hasMetadata(ResourceMetadata, Test);
		assert.equal(actualHasMetadata, false, "Shouldn't have metadata");
	});

	it("Applies metadata", function () {
		function middleware1(request: Request) {
			return null;
		}

		function middleware2(request: Request) {
			return null;
		}

		let route = {
			method: HttpMethods.GET,
			path: "/test",
			middleware: [middleware2]
		};

		
		@Resource({
			basePath: "/testResource",
			middleware: [middleware1]
		})
		class TestResource {
			@Route(route)
			method() { }
		}

		let expected = [{
			route,
			constructor: TestResource,
			handler: "method",
			queryParams: [],
			routeParams: [],
			bodyParams: []
		}];
		
		let actualHasMetadata = Reflect.hasMetadata(ResourceMetadata, TestResource);
		assert.equal(actualHasMetadata, true, "Metadata must be applied");

		let actualGetMetadata = Reflect.getMetadata(ResourceMetadata, TestResource);
		assert.deepStrictEqual(actualGetMetadata, expected, "");
		assert.equal(actualGetMetadata[0].route.middleware.length, 2, "Middleware not applied correctly");
	});

	it("Applies paremeter metadata", function () {
		let route = {
			method: HttpMethods.GET,
			path: "/test"
		};

		
		@Resource({
			basePath: "/testResource",
			resolvers: new Map([["value", () => undefined]]),
		validators: new Map([["value", /.*/]])
	})
	class TestResource {
		@Route(route)
		method(@RouteParam() value: Number) { }
	}

	let expected = [{
		route,
		constructor: TestResource,
		handler: "method",
		queryParams: [],
		routeParams: [{
			name: "value",
			validator: /.*/,
			resolver: undefined
		}],
		bodyParams: []
	}];
	
		let actualHasMetadata = Reflect.hasMetadata(ResourceMetadata, TestResource);
		assert.equal(actualHasMetadata, true, "Metadata must be applied");

		let actualGetMetadata = Reflect.getMetadata(ResourceMetadata, TestResource);
		assert.deepStrictEqual(expected, actualGetMetadata, "Metadata not formatted right");
	});

	it("Applies query parameter metadata", function () {
		function validate(obj) { return false; }
		let route = {
			method: HttpMethods.GET,
			path: "/test"
		};

		
		@Resource({
			basePath: "/testResource",
			validators: new Map([["value", validate]])
		})
		class TestResource {
			@Route(route)
			method(@QueryParam() value: Number) { }
		}

		let expected = [{
			route,
			constructor: TestResource,
			handler: "method",
			queryParams: [{
				name: "value",
				validator: validate
			}],
			routeParams: [],
			bodyParams: []
		}];
		
		let actualHasMetadata = Reflect.hasMetadata(ResourceMetadata, TestResource);
		assert.equal(actualHasMetadata, true, "Metadata must be applied");

		let actualGetMetadata = Reflect.getMetadata(ResourceMetadata, TestResource);
		assert.deepStrictEqual(actualGetMetadata, expected, "Metadata not formatted right");
	});

	it("Applies body parameter metadata", function () {
		function validate(obj) { return false; }
		let route = {
			method: HttpMethods.GET,
			path: "/test"
		};

		
		@Resource({
			basePath: "/testResource",
			validators: new Map([["value", validate]])
		})
		class TestResource {
			@Route(route)
			method(@BodyParam() value: Number) { }
		}
		
		let expected = [{
			route,
			constructor: TestResource,
			handler: "method",
			bodyParams: [{
				name: "value",
				validator: validate
			}],
			routeParams: [],
			queryParams: []
		}];
		
		let actualHasMetadata = Reflect.hasMetadata(ResourceMetadata, TestResource);
		assert.equal(actualHasMetadata, true, "Metadata must be applied");

		let actualGetMetadata = Reflect.getMetadata(ResourceMetadata, TestResource);
		assert.deepStrictEqual(actualGetMetadata, expected, "Metadata not formatted right");
	});
});