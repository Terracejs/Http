import * as assert from "assert";
import { Route, RouteParam, ParameterMetadata, staticResolver, HttpMethods } from "@Terracejs/Http";

describe("RouteParam Annotation", function () {
	it("sets the static resolver", function () {
		let expected = { name: "foo" };

		class Test {
			method(@RouteParam(expected) temp: string) {}
		}

		let hasMeta = Reflect.hasMetadata(ParameterMetadata, Test.prototype.method);
		assert.equal(hasMeta, true, "Metadata not applied");

		let metaData = Reflect.getMetadata(ParameterMetadata, Test.prototype.method);
		assert.equal(metaData[0].name, expected.name, "Incorrect param name");
		assert.notEqual(metaData[0].validator, undefined, "Validator must be applied");

		assert.equal(metaData[0].resolver, staticResolver, "Static resolver not set");
	});
});