import * as assert from "assert";
// import * as TypeMoq from "typemoq";
import { Mock, IMock, Times} from "typemoq";
import { CreateParamDecorator, RequestParam, HttpMethods } from "@Terracejs/Http";

describe("RequestParam Annotation", function () {
	it("applies default parameter metadata", function () {
		let key = Symbol("TempKey");
		const decorator = CreateParamDecorator(key); 

		class Test {
			method(@decorator() temp: string) {}
		}

		let hasMeta = Reflect.hasMetadata(key, Test.prototype.method);
		assert.equal(hasMeta, true, "Metadata not applied");
		
		let metaData = Reflect.getMetadata(key, Test.prototype.method);
		assert.equal(metaData[0].name, "temp", "Incorrect param name");
		assert.notEqual(metaData[0].validator, undefined, "Validator must exists");
	});

	it("provides default name parameter when not provided", function () {
		const key = Symbol("TempKey");
		const decorator = CreateParamDecorator(key);
		let expected = { validator: obj => false };

		class Test {
			method(@decorator(expected) temp: string) {}
		}

		let hasMeta = Reflect.hasMetadata(key, Test.prototype.method);
		assert.equal(hasMeta, true, "Metadata not applied");

		let metaData = Reflect.getMetadata(key, Test.prototype.method);
		assert.equal(metaData[0].name, "temp", "Incorrect param name");
		assert.equal(metaData[0].validator, expected.validator, "Incorrect validator applied");
	});

	it("provides default validator parameter when not provided", function () {
		const key = Symbol("TempKey");
		const decorator = CreateParamDecorator(key);
		let expected = { name: "foo" };

		class Test {
			method(@decorator(expected) temp: string) {}
		}

		let hasMeta = Reflect.hasMetadata(key, Test.prototype.method);
		assert.equal(hasMeta, true, "Metadata not applied");

		let metaData = Reflect.getMetadata(key, Test.prototype.method);
		assert.equal(metaData[0].name, expected.name, "Incorrect param name");
		assert.notEqual(metaData[0].validator, undefined, "Validator must be applied");
	});

	it("calls the provided validator", function () {
		const key = Symbol("TempKey");
		const validator = Mock.ofInstance(function (config: RequestParam) {});
		const decorator = CreateParamDecorator(key, validator.object);
		let expected = { name: "foo", validator: temp => true };

		class Test {
			method(@decorator(expected) temp: string) {}
		}

		let hasMeta = Reflect.hasMetadata(key, Test.prototype.method);
		assert.equal(hasMeta, true, "Metadata not applied");

		let metaData = Reflect.getMetadata(key, Test.prototype.method);
		assert.equal(metaData[0].name, expected.name, "Incorrect param name");
		assert.notEqual(metaData[0].validator, validator.object, "Validator must be applied");

		validator.verify(x => x(expected), Times.once());
	});
});