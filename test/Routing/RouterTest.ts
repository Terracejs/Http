import { Router, RouterMetada } from "index";
import { equal, deepStrictEqual } from "assert";

describe("Router Annotation", function () {
	it("Adds metadata", function () {
		@Router()
		class Test {

		}

		let hasMeta = Reflect.hasMetadata(RouterMetada, Test);
		equal(true, hasMeta, "Metadata not applied");

		let actual: Router = Reflect.getMetadata(RouterMetada, Test);
		deepStrictEqual([".*"], actual.host, "Host should be set");
	});
});