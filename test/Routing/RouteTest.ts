import * as assert from "assert";
import { Route, RouteMetadata, HttpMethods } from "@Terracejs/Http";

describe("Route Annotation", function () {
	it("Applies metadata", function () {
		let expected = { method: HttpMethods.GET, path: "" };
		
		class Test {
			@Route(expected)
			method() {}
		}

		let actualHasMetadata = Reflect.hasMetadata(RouteMetadata, Test.prototype);
		let actualGetMetadata: Map<String, Route> = Reflect.getMetadata(RouteMetadata, Test.prototype);

		assert.equal(actualHasMetadata, true, "Metadata must be applied");
		assert.equal(actualGetMetadata.get("method"), expected, "Metadata must match");
	});

	it("Errors when placed on a static method", function () {
		let expected = { method: HttpMethods.GET, path: "" };

		assert.throws(function () {
			class Test {
				@Route(expected)
				static method() {}
			}
		}, "Should have errored");
	});
});